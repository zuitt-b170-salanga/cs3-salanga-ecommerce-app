/* src > components > TreatmentCard.js */

import {Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';




export default function TreatmentCard ({treatmentProp}) {


	const {_id, name, description, price} = treatmentProp

	return(


		<Card>

				<Card.Body> 	

					<Card.Title>{name}</Card.Title>	
					<Card.Subtitle>Description:</Card.Subtitle>					
					<Card.Text>{description}</Card.Text>
                	<Card.Subtitle>Price:</Card.Subtitle>
                	<Card.Text>USD {price}</Card.Text>

                	<Link className="btn btn-primary" to={`/treatments/${_id}`}>Details</Link>

				</Card.Body>

		</Card>
	)
};

//====================================================









