/* src > data > treatmentsData.js */


export default[
    
    {
        id: "CPT97112  ",
        name: "Neuromuscular Reeducation",
        description: "Neuromuscular reeducation of movement, balance, coordination, kinesthetic sense, posture, and/or proprioception for sitting and/or standing activities.",
        price: 40.42,
        onOffer: true
    },

	{
        id: "CPT97035",
        name: "Light Therapy/Phototherapy",
        description: "Promotes tissue repair and natural healing in and around bones, joints, muscles, ligaments and tendons.",
        price: 200,
        onOffer: true
    },

    {
        id: "CPT97024",
        name: "Diathermy",
        description: "Uses a high-frequency electric current to stimulate heat generation within body tissues.",
        price: 155,
        onOffer: true
    },

    {
        id: "CPT97140",
        name: "Manual therapy",
        description: "The skilled application of passive movement to a joint either within or beyond its active range of movement.",
        price: 76,
        onOffer: true
    },

    {
        id: "CPT97150",
        name: "Group therapy",
        description: "The treatment of 2-6 patients, who are performing the same or similar activities and are supervised by a therapist or an assistant.",
        price: 40,
        onOffer: false
    },

    {
        id: "CPT97012",
        name: "Mechanical Traction",
        description: "The force used to create a degree of tension of soft tissues and/or to allow for separation between joint surfaces.",
        price: 100,
        onOffer: true
    }

]

