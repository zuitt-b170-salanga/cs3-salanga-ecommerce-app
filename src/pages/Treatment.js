/* src > pages > Treatment.js */

import {useState, useEffect, useContext} from 'react';
import Container from 'react-bootstrap/Container';

import AdminView from '../components/AdminView.js';
import UserView from '../components/UserView.js';

// context
import UserContext from '../UserContext';


export default function Treatment() {


	const { user } = useContext(UserContext);
	const [treatments, setTreatments] = useState([]); 

	const fetchData = () => {
		// let token = localStorage.getItem('token')

		fetch('https://lit-plateau-63513.herokuapp.com/api/treatments')
		// fetch('http://localhost:4000/api/treatments')
			.then(res => res.json())
			.then(data => {

				setTreatments(data);
		});
	}

	useEffect(() => {
		fetchData();
	}, []);


	return (
		<Container className="p-4">
			{(user.isAdmin === true) ? 
	
				<AdminView treatmentsData={treatments} fetchData={fetchData}/>
				:
				<UserView treatmentsData={treatments}/>
			} 
		</Container>
	)
}




//==========================================================









